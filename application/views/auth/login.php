<?php $this->load->view('frontend/includes/header')?>
<?php echo form_open('auth/login') ?>
		<div class="columns">
		  <div class="column">

		  </div>

		  <div class="column">
		    <label class="label">Email</label>
				<p class="control has-icon has-icon-right">
				  <input name="email" class="input" type="email" placeholder="Email">
          <span class="icon is-small">
            <i class="fa fa-warning"></i>
          </span>
          <?php echo form_error('email'); ?>
				  <!-- <span class="help is-danger">This email is invalid</span> -->
				</p>

				<label class="label">Password</label>
				<p class="control has-icon has-icon-right">
				  <input name="password" class="input" type="password" placeholder="Password">
          <span class="icon is-small">
            <i class="fa fa-warning"></i>
          </span>
          <?php echo form_error('email'); ?>
				  <!-- <span class="help is-danger">This email is invalid</span> -->
				</p>

				<p class="control">
				  <label class="checkbox">
				    <input type="checkbox">
				    Remember me
				  </label>
				</p>
				<div class="control is-grouped">
				  <p class="control">
				    <button class="button is-primary">Login</button>
				  </p>
				</div>
		  </div>
		  <div class="column">

		  </div>
		</div>
<?php echo form_close() ?>
<?php $this->load->view('frontend/includes/footer')?>
