<div class="notification is-default">
<aside class="menu backend">
  <p class="menu-label">
    General
  </p>
  <ul class="menu-list">
    <li><a href="/admin/dashboard">Dashboard</a></li>
    <li><a href="/admin/journals">Journals</a></li>
    <li><a href="/admin/articles">Articles</a></li>
  </ul>

  <p class="menu-label">
    Transactions
  </p>
  <ul class="menu-list">
    <li><a>Payments</a></li>
    <li><a>Transfers</a></li>
    <li><a>Balance</a></li>
  </ul>
</aside>
</div>
