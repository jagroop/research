<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
	<title>JMS | Dashboard</title>
	<link rel="stylesheet" href="<?php echo asset_url() ?>css/bulma.css">
	<link rel="stylesheet" href="<?php echo asset_url() ?>css/style.css">
	<script
  	src="https://code.jquery.com/jquery-3.1.1.min.js"
  	integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  	crossorigin="anonymous"></script>
</head>
<body>
<header><?php $this->load->view('backend/includes/nav')?></header>
<section>