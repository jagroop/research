<?php $this->load->view('backend/includes/header')?>
  <div class="columns">
    <div class="column is-one-quarter">
      <!-- sidebar -->
        <?php $this->load->view('backend/includes/sidebar')?>
      <!-- #sidebar -->
    </div>
    <div class="column table">
      <!-- Add Journal Form -->
      <?php echo form_open('admin/journals/update/' . $journal->id) ?>
        <div class="field">
          <label class="label">Journal Title:</label>
          <p class="control">
            <input class="input" name="title" type="text" value="<?php echo $journal->title ?>" placeholder="Enter journal title here...">
            <?php echo form_error('title'); ?>
          </p>
        </div>
        <br>
        <div class="field">
          <label class="label">Journal Description:</label>
          <p class="control">
            <textarea class="textarea" name="description" type="text" placeholder="Enter journal description here..."><?php echo $journal->description ?></textarea>
            <?php echo form_error('description'); ?>
          </p>
        </div>

        <br>
        <div class="field">
          <p class="control">
            <input class="button is-primary" type="submit" value="Update">
          </p>
        </div>
      <?php echo form_close() ?>
      <!-- #Add Journal Form -->

    </div>
  </div>
<?php $this->load->view('backend/includes/footer')?>
