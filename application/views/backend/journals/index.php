<?php $this->load->view('backend/includes/header')?>
	<div class="columns">
	  <div class="column is-one-quarter">
	    <!-- sidebar -->
	    	<?php $this->load->view('backend/includes/sidebar')?>
	    <!-- #sidebar -->
	  </div>
	  <div class="column table">
    <a class="button is-success is-pulled-right" href="/admin/journals/create">Add Journal</a>
			<!-- Journals listing -->
        <table class="table">
          <thead>
            <tr>
              <th><abbr title="Journal Id">#</abbr></th>
              <th><abbr title="Journal Name">Journal Name</abbr></th>
              <th>Published On</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </thead>

          <tbody>
          <?php if (count($journals)): ?>
              <?php foreach ($journals as $key => $journal): ?>
                <tr>
                  <td><?php echo $key + 1 ?></td>
                  <td><?php echo $journal->title ?></td>
                  <td><?php echo date('F j, Y, g:i a', strtotime($journal->created_at)) ?></td>
                  <td><em><?php echo ($journal->status == 1) ? '<span class="tag is-success">Active</span>' : '<span class="tag is-danger">Deactive</span>' ?></em></td>
                  <td>
                    <div class="field is-grouped">
                      <?php echo form_open('admin/journals/delete') ?>
                        <input type="hidden" name="journal" value="<?php echo $journal->id ?>">
                        <button name="delete" onclick="return confirm('Are you sure ?');" class="button is-info">Delete</button>
                        <a href="/admin/journals/edit/<?php echo $journal->id ?>" class="button is-info">Edit</a>
                      <?php echo form_close() ?>
                    </div>
                  </td>
                </tr>
              <?php endforeach;?>
          <?php else: ?>
              <tr><td colspan="5" class="center">No journals found.</td></tr>
          <?php endif;?>
          </tbody>
        </table>
			<!-- #Journals listing -->
	  </div>
	</div>
<?php $this->load->view('backend/includes/footer')?>
