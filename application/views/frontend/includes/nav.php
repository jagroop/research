
<nav class="nav has-shadow">
  <div class="container">
    <div class="nav-left">
      <a href="/" class="nav-item">
        <img src="<?php echo asset_url() ?>images/logo.png" alt="JMS">
      </a>
      <a href="/" class="nav-item is-tab is-hidden-mobile is-active">Home</a>
      <a href="/home/about" class="nav-item is-tab is-hidden-mobile">About</a>
      <a href="/home/contact" class="nav-item is-tab is-hidden-mobile">Contact</a>
    </div>
    <span class="nav-toggle">
      <span></span>
      <span></span>
      <span></span>
    </span>
    <div class="nav-right nav-menu">
    <?php if ($this->session->userdata('is_logged_in')): ?>
      <?php if ($this->session->userdata('is_logged_in') == 3): ?>
        <a href="/admin/dashboard" class="nav-item is-tab">Dashboard</a>
      <?php endif;?>
      <a href="/home/logout" class="nav-item is-tab">Logout</a>
    <?php else: ?>
      <a href="/home/login" class="nav-item is-tab">Login</a>
    <?php endif;?>
    </div>
  </div>
</nav>
