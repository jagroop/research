<?php $this->load->view('frontend/includes/header')?>
    <h1 class="title"><i class="fa fa-sticky-note-o" aria-hidden="true"></i> Articles</h1>
  <div class="box">
    <?php foreach ($articles as $article): ?>
      <div class="article">
          <div class="heading">
          <h1 class="title"><?php echo $article->title ?></h1>
          <h2 class="subtitle">
            <a class="button is-info is-outlined"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>  <span class="with-i">PDF</span></a>
          </h2>
        </div>
      </div>
  <?php endforeach;?>
  </div>
<?php $this->load->view('frontend/includes/footer')?>
