<?php $this->load->view('frontend/includes/header')?>
  <?php foreach ($journals as $journal): ?>
      <div class="article journal">
      	  <div class="heading">
	        <h1 class="title"><?php echo $journal->title ?></h1>
	        <h2 class="subtitle">
	          <a href="/home/journal/<?php echo $journal->id ?>/<?php echo $journal->title_slug ?>" class="button is-info">View Journal</a>
	          <a class="button is-info">Current Issue</a>
	        </h2>
	      </div>
      </div>
  <?php endforeach;?>

<?php $this->load->view('frontend/includes/footer')?>
