<?php $this->load->view('frontend/includes/header')?>
		<div class="columns">
		  <div class="column">

		  </div>

		  <div class="column">

		  <div class="article">
	      	  <div class="heading">
		        <h1 class="title">Contact Us</h1>
		        <h2 class="subtitle">
		          <label class="label">Email</label>
				<p class="control has-icon has-icon-right">
				  <input class="input" type="email" placeholder="Email">
				  <span class="icon is-small">
				    <i class="fa fa-warning"></i>
				  </span>
				  <!-- <span class="help is-danger">This email is invalid</span> -->
				</p>

				<label class="label">Subject</label>
				<p class="control has-icon has-icon-right">
				  <input class="input" type="text" placeholder="Password">
				  <span class="icon is-small">
				    <i class="fa fa-warning"></i>
				  </span>
				  <!-- <span class="help is-danger">This email is invalid</span> -->
				</p>

				<label class="label">Message</label>
				<p class="control has-icon has-icon-right">
				  <textarea class="input textarea" placeholder="Enter your Message here..."></textarea>
				  <span class="icon is-small">
				    <i class="fa fa-warning"></i>
				  </span>
				  <!-- <span class="help is-danger">This email is invalid</span> -->
				</p>

				<div class="control is-grouped">
				  <p class="control">
				    <button id="sendMessage" class="button is-large is-info" onclick="">Send</button>
				  </p>
				</div>
		        </h2>
		      </div>
	      </div>

		  </div>

		  <div class="column">

		  </div>
		</div>
<script>
	$( document ).ready(function(){
		$("#sendMessage").click(function(e){
			e.preventDefault();
			$(this).addClass("is-loading");
		});
	});
</script>
<?php $this->load->view('frontend/includes/footer')?>