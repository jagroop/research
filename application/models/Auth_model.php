<?php
class Auth_model extends CI_Model {

	public function attempt($email, $password) {
		$this->db->where('email', $email);
		$this->db->where('password', sha1($password));
		$q = $this->db->get('users');
		$user = $q->result();
		return (count($user)) ? $user[0] : null;
	}
}
