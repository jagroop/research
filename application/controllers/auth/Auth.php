<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends My_Controller {

	protected function redirectUser($role) {
		switch ($role) {
		case 1:
			return redirect('home/index', 'refresh');
			break;

		case 2:
			die('under progress');
			break;

		case 3:
			return redirect('admin/dashboard', 'refresh');
			break;
			break;

		default:
			die('Unknown User Role tried to authenticate.');
			break;
		}
	}
	protected function authenticate($email, $password) {
		$getUser = $this->Auth_model->attempt($email, $password);
		if (!$getUser) {
			return $this->load->view('auth/login', ['login_error']);
		}
		//Create a session
		$this->session->set_userdata('name', $getUser->name);
		$this->session->set_userdata('is_logged_in', $getUser->user_role);
		return $this->redirectUser((int) $getUser->user_role);
	}

	public function __construct() {
		parent::__construct();
		$this->load->model('Auth_model');
	}

	public function login() {
		if ($this->input->method(TRUE) == 'POST') {
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');

			if ($this->form_validation->run() == FALSE) {
				return $this->load->view('auth/login');
			} else {
				//Authenticate
				$email = $this->input->post('email');
				$password = $this->input->post('password');
				return $this->authenticate($email, $password);
			}
		}
		return redirect('home/login', 'refresh');
	}

	public function logout() {
		return $this->load->view('backend/dashboard');
	}

}
