<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Journals extends My_Controller {

	public function __construct() {
		parent::__construct();
		$this->auth('admin');
	}

	/**
	 * Listing all Journals
	 * @return View
	 */
	public function index() {
		$journals = $this->db->get('journals')->result();
		return $this->load->view('backend/journals/index', compact('journals'));
	}

	/**
	 * Load view to create a journal
	 * @return View
	 */
	public function create() {
		return $this->load->view('backend/journals/create');
	}

	/**
	 * Load view to create a journal
	 * @return View
	 */
	public function edit($id) {
		$id = (int) $id;
		$journal = $this->db->get_where('journals', ['id' => $id])->result();
		if (!count($journal)) {
			return redirect('backend/journals/index', 'refresh');
		}
		$journal = $journal[0];
		return $this->load->view('backend/journals/edit', compact('journal'));

	}

	/**
	 * Store a Journal to the DB
	 * @return Redirect
	 */
	public function store() {
		if ($this->input->method(TRUE) == 'POST') {
			$this->form_validation->set_rules('title', 'Journal Title', 'required|is_unique[journals.title]');

			if ($this->form_validation->run() == FALSE) {
				return $this->load->view('backend/journals/create');
			} else {
				//Authenticate
				$title = $this->input->post('title');
				$slug = str_slug($title);
				$desc = $this->input->post('description');
				$date = date('Y-m-d H:i:s');
				$this->db->insert('journals',
					[
						'title' => $title,
						'title_slug' => $slug,
						'description' => $desc,
						'created_at' => $date,
					]);
			}
		}
		return redirect('admin/journals/index', 'refresh');
	}

	/**
	 * Update a Journal in the DB
	 * @return Redirect
	 */
	public function update($id) {
		if ($this->input->method(TRUE) == 'POST') {
			$this->form_validation->set_rules('title', 'Journal Title', 'required|edit_unique[journals.title.' . $id . ']');

			if ($this->form_validation->run() == FALSE) {
				return $this->load->view('backend/journals/edit');
			} else {
				$title = $this->input->post('title');
				$slug = str_slug($title);
				$desc = $this->input->post('description');
				$date = date('Y-m-d H:i:s');
				$this->db->where('id', $id);
				$this->db->update('journals',
					[
						'title' => $title,
						'title_slug' => $slug,
						'description' => $desc,
					]);
			}
		}
		return redirect('admin/journals/index', 'refresh');
	}

	public function delete() {
		if ($this->input->method(TRUE) == 'POST') {
			$this->form_validation->set_rules('journal', 'Journal ID', 'required');

			if ($this->form_validation->run() == FALSE) {
				return $this->load->view('backend/journals/index');
			} else {
				//Authenticate
				$id = (int) $this->input->post('journal');
				$this->db->where('id', $id);
				$this->db->delete('journals');
			}
		}
		return redirect('admin/journals/index', 'refresh');
	}

}
