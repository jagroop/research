<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends My_Controller {

	public function __construct() {
		parent::__construct();
		$this->auth('admin');
	}

	public function index() {
		return $this->load->view('backend/dashboard');
	}

}
