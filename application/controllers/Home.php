<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index() {
		$journals = $this->db->get('journals')->result();
		return $this->load->view('frontend/index', compact('journals'));
	}

	public function login() {
		return $this->load->view('auth/login');
	}

	public function logout() {
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('is_logged_in');
		return redirect('home/index', 'refresh');
	}

	public function contact() {
		return $this->load->view('frontend/pages/contact');
	}

	public function about() {
		return $this->load->view('frontend/pages/about');
	}

	public function journal($id, $slug = "") {
		$this->db->select('articles.*');
		$this->db->from('articles');
		$this->db->join('journals', 'journals.id = articles.journal_id');
		$articles = $this->db->get()->result();
		return $this->load->view('frontend/articles/index', compact('articles'));
	}

}
