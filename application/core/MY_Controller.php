<?php

class MY_Controller extends CI_Controller {

	protected $guards = ['1' => 'user', '2' => 'author', '3' => 'admin'];

	public function __construct() {
		parent::__construct();
	}

	/**
	 * User Authentication Guard
	 * @param  string $guard User guard
	 * @return boolean || redirect
	 */
	protected function auth($guard = 'user') {
		if (!in_array($guard, array_values($this->guards))) {
			throw new Exception("Unknown Guard " . $guard, 1);
			return;
		}
		$flipGuards = array_flip($this->guards);
		$loggedIn = $this->session->userdata('is_logged_in');
		$who = @$flipGuards[$guard];
		if (!$loggedIn || $loggedIn != $who) {
			//Not logged in
			return redirect('/');
		}
		//yes logged in
		return true;
	}

}
